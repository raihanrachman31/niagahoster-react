import React, { useState, Fragment, Component } from 'react';

class CostSavingHome extends Component {
  state = {
    data : [
      {
        id: 1,
        title : "Harga Murah Fitur Lengkap",
        description: "Anda bisa berhemat dan tetap mendapatkan hosting terbaik dengan fitur lengkap, dari auto install WordPress, cPanel lengkap, hingga SSL gratis"
      },
      {
        id: 2,
        title : "Website Selalu Online",
        description:"Jaminan server uptime 99,98% memungkinkan website Anda selalu online sehingga Anda tidak perlu khawatir kehilangan trafik dan pendapatan."
      },
      {
        id: 3,
        title : "Tim Support Andal dan Cepat Tanggap",
        description: "Tidak perlu menunggu lama, selesaikan masalah Anda dengan cepat secara real time melalui live chat 24/7"
      }
    ]
  }
  render(){
    return(
      <section class="cost-saving-wrapper">
          <div class="container">
            <h3 class="heading">
              Biaya Hemat Kualitas Hebat
            </h3>
            <div class="cost-saving-content-wrapper flex">

              <div class="cost-saving-image-wrapper">
                <img src={require("../assets/images/titis.webp")} alt="" class="cost-titis-img"/>
                <img src={require("../assets/images/woman1.webp")} alt="" class="cost-woman1-img"/>
                <img src={require("../assets/images/woman2.png")} alt="" class="cost-woman2-img"/>
                <img src={require("../assets/images/man.png")} alt="" class="cost-man-img"/>
                <img src={require("../assets/images/icon-online.svg")} alt="" class="cost-online-img"/>
                <img src={require("../assets/images/intercom-logo.svg")} alt="" class="cost-intercom-img"/>
              </div>

              <div class="cost-saving-feature-wrapper">
              {this.state.data.map( item => {
                  return (
                    <div class="cost-saving-feature">
                      <h4 class="cost-saving-feature-title">{item.title}</h4>
                      <p class="cost-saving-description">{item.description}</p>
                    </div>
                  )
                })
              }

              </div>	
            </div>
          </div>
        </section>
      )
  }
}

export default CostSavingHome;