import React, { useState, Fragment, Component } from 'react';

class CustomerHome extends Component {
  state = {
    data : [
      {
        image: require("../assets/images/devjavu.webp"),
        testimony: "Website itu sangat penting bagi UMKM sebagai sarana promosi untuk memenangkan persaingan di era digital.",
        ownerName: "Didik & Johan - ",
        ownerOf:"Owner Devjavu",
      },
      {
        image: require("../assets/images/optimizer.webp"),
        testimony: "Bagi saya Niagahoster bukan sekadar penyedia hosting, melainkan partner bisnis yang bisa dipercaya.",
        ownerName: "Bob Setyo - ",
        ownerOf: "Owner Digital Optimizer Indonesia",
      },
      {
        image: require("../assets/images/sateratu.webp"),
        testimony: "Solusi yang diberikan tim support Niagahoster sangat mudah dimengerti buat saya yang tidak paham teknis.",
        ownerName: "Budi Seputro - ",
        ownerOf: "Owner Sate Ratu",
      },
    ]
  }
  render(){
    return(
      <section class="customer-wrapper">
        <div class="container">
          <h3 class="heading">
            Kata Pelanggan Tentang Niagahoster
          </h3>
          <div class="customer-content-wrapper flex">
            {this.state.data.map( item => {
              return (
                <div class="customer">
                  <img src={item.image} alt="" class="devjavu-img"/>
                  <p class="customer-testimony">{item.testimony}</p>
                  <p class="owner">{item.ownerName}<span>{item.ownerOf}</span></p>
                </div>
              )
            })}
          </div>
        </div>
      </section>
    )
  }
}

export default CustomerHome;
