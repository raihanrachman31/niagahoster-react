import React, { useState } from 'react';

const Footer = ()=> {
  return(
      <footer>
				<div class="container">
					
					<div class="footer-content-wrapper flex">

						<div class="footer-content-list-wrapper">
							<h5 class="footer-content-title">Hubungi Kami</h5>
							<p class="footer-content-list">Telp: 0274-2885822</p>
							<p class="footer-content-list">WA: 0895422447394</p>
							<p class="footer-content-list">Senin - Minggu</p>
							<p class="footer-content-list">24 Jam Non Stop</p>
							<p class="footer-content-list visibility-none">Spacer</p>
							<p class="footer-content-list">Jl. Palagan Tentara Pelajar</p>
							<p class="footer-content-list">No 81 Jongkang, Sariharjo,</p>
							<p class="footer-content-list">Ngaglik, Sleman</p>
							<p class="footer-content-list">Daerah Istimewa Yogyakarta</p>
							<p class="footer-content-list">55581</p>
						</div>

						<div class="footer-content-list-wrapper">
							<h5 class="footer-content-title">Layanan</h5>
							<p class="footer-content-list">Domain</p>
							<p class="footer-content-list">Shared Hosting</p>
							<p class="footer-content-list">Cloud Hosting</p>
							<p class="footer-content-list">Cloud VPS hosting</p>
							<p class="footer-content-list">Transfer Hosting</p>
							<p class="footer-content-list">Web Builder</p>
							<p class="footer-content-list">Keamanan SSL/HTTPS</p>
							<p class="footer-content-list">Jasa Pembuatan Website</p>
							<p class="footer-content-list">Program Afiliasi</p>
							<p class="footer-content-list">Whois</p>
							<p class="footer-content-list">Niagahoser Status</p>
						</div>

						<div class="footer-content-list-wrapper">
							<h5 class="footer-content-title">Service Hosting</h5>
							<p class="footer-content-list">Hosting Murah</p>
							<p class="footer-content-list">Hosting Indonesia</p>
							<p class="footer-content-list">Hosting Singapore SG</p>
							<p class="footer-content-list">Hosting Wordpress</p>
							<p class="footer-content-list">Email Hosting</p>
							<p class="footer-content-list">Reseller Hosting</p>
							<p class="footer-content-list">Web Hosting Unlimited</p>
						
						</div>

						<div class="footer-content-list-wrapper">
							<h5 class="footer-content-title">Kenapa Pilih Niagahoster?</h5>
							<p class="footer-content-list">Hosting Terbaik</p>
							<p class="footer-content-list">Datacenter Hosting Terbaik</p>
							<p class="footer-content-list">Domain Gratis</p>
							<p class="footer-content-list">Bagi-bagi Domain Gratis</p>
							<p class="footer-content-list">Bagi-bagi Hosting Gratisv</p>
							<p class="footer-content-list">Review Pelanggan</p>
							</div>
						
					</div>

					<div class="footer-content-wrapper flex">
						<div class="footer-content-list-wrapper">
							<h5 class="footer-content-title">Tutorial</h5>
							<p class="footer-content-list">Ebook Gratis</p>
							<p class="footer-content-list">Knowledgebase</p>
							<p class="footer-content-list">Blog</p>
							<p class="footer-content-list">Cara Pembayaran</p>
							<p class="footer-content-list">Niaga Course</p>
						</div>

						<div class="footer-content-list-wrapper">
							<h5 class="footer-content-title">Tentang Kami</h5>
							<p class="footer-content-list">Tentang</p>
							<p class="footer-content-list">Penawaran & Promo Spesial</p>
							<p class="footer-content-list">Niaga Poin</p>
							<p class="footer-content-list">Karir</p>
							<p class="footer-content-list">kontak Kami</p>
						</div>

						<form class="footer-content-list-wrapper">
							<h5 class="footer-content-title">Newsletter</h5>
							<input type="email" placeholder="youremail@mail.com" name="email" id="email"/>
							<hr/>
							<div class="newsletter-btn-wrapper">
								<a href="" class="btn-newsletter orange">berlangganan</a>
							</div>
						</form>

						<div class="footer-content-list-wrapper">
							<div class="footer-icon-wrapper">
								<a href=""><i class="fab fa-facebook-f"></i></a>
								<a href=""><i class="fab fa-instagram"></i></a>
								<a href=""><i class="fab fa-linkedin-in"></i></a>
								<a href=""><i class="fab fa-twitter"></i></a>
							</div>
						</div>
					</div>
					
					<p class="copyright">Copyright ©2019 Niagahoster | Hosting powered by PHP7, CloudLinux, CloudFlare, BitNinja and DC DCI-Indonesia. Cloud VPS Murah powered by Webuzo Softaculous, Intel SSD and cloud computing technology</p>
					<a href="" class="terms-condition">Syarat dan Ketentuan | Kebijakan Privasi</a>
				</div>
			</footer>
  )
}

export default Footer;