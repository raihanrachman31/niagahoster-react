import React from 'react';
import HomeNiaga from '../views/HomeNiaga';
import Header from '../layout/Header';
import Footer from '../layout/Footer';
import OfferingHome from '../components/OfferingHome';



const HomePage = ()=> {
  return(
    <div>
      <Header/>
      <HomeNiaga/>
      <OfferingHome/>
      <Footer/>
    </div>
  )
}

const UnlimitedPage = ()=> {
  return(
    <div>
      <Header/>
        Unlimited but limited like ur quota
        <OfferingHome/>
      <Footer/>
    </div>
  )
}

const HostingPage = ()=> {
  return(
    <div>
      <Header/>
        a Host
      <OfferingHome/>
      <Footer/>
    </div>
  )
}

const VpsPage = ()=> {
  return(
    <div>
      <Header/>
        just V
        <OfferingHome/>
      <Footer/>
    </div>
  )
}

const DomainPage = ()=> {
  return(
    <div>
      <Header/>
        this is dom
        <OfferingHome/>
      <Footer/>
    </div>
  )
}

const AfiliasiPage = ()=> {
  return(
    <div>
      <Header/>
      this is af
      <OfferingHome/>
      <Footer/>
    </div>
  )
}

const BlogPage = ()=> {
  return(
    <div>
      <Header/>
      this blo
      <OfferingHome/>
      <Footer/>
    </div>
  )
}

export {HomePage, UnlimitedPage, HostingPage, VpsPage, DomainPage, AfiliasiPage, BlogPage};