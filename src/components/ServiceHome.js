import React, { useState, Fragment, Component } from 'react';

class ServiceHome extends Component {
  state = {
    data : [
      {
        id: 1,
        serviceName : "Unlimited Hosting",
        image: require("../assets/images/icon-1.svg"),
        description: "Cocok untuk website skala kecil",
        price:"Rp 10.000,-"
      },
      {
        id: 2,
        serviceName : "Cloud Hosting",
        image: require("../assets/images/icons-cloud-hosting.svg"),
        description: "Kapasitas resource tinggi,fully managed dan mudah dikelola",
        price:"Rp 150.000,-"
      },
      {
        id: 3,
        serviceName : "Cloud VPS",
        image: require("../assets/images/icons-cloud-vps.svg"),
        description: "Dedicated resource dengan akses root dan konfigurasi mandiri",
        price:"Rp 104.000,-"
      },
      {
        id: 4,
        serviceName : "Domain",
        image: require("../assets/images/icons-domain.svg"),
        description: "Temukan nama domain yang Anda inginkan",
        price: "Rp 14.000,-"
      }
    ]
  }

  render(){
    return(
      <section className="layanan-wrapper">
            <div className="container">
              <h3 className="heading">
                Layanan Niagahoster
              </h3>
              <div className="card-wrapper">
                <div className="card-column-wrapper flex">
                  {this.state.data.map(item => {
                    return(
                      <div className="card layanan" key={item.id}>
                        <figure className="layanan-icon-wrapper">
                          <img src={item.image} alt="" className="layanan-icon"/>
                        </figure>
                        <figcaption>{item.serviceName}</figcaption>
                        <p className="layanan-deskripsi">{item.description}</p>
                        <p className="layanan-price-intro">Mulai dari</p>
                        <p className="layanan-price">{item.price}</p>	
                      </div>
                    )
                    })}
              </div>
              <div className="card layanan-row">
                <figure className="layanan-icon-wrapper">
                  <img src={require("../assets/images/icons-cloud-hosting.svg")} alt="" className="layanan-icon"/>
                </figure>
                <div className="description-wrapper-row-card">
                  <figcaption>Pembuatan Website</figcaption>
                  <p className="layanan-deskripsi">500 perusahaan lebih percayakan pembuatan websitenya pada kami	<a href="">Cek selengkapnya...</a></p>
                </div>
              </div>
            </div>
          </div>
        </section>
    )
  }
  
}

export default ServiceHome;