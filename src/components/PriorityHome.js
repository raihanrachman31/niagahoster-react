import React, { useState, Fragment } from 'react';

const PriorityHome = ()=> {
  return(
    <section className="priority-wrapper">
      <div className="container">
        <h3 className="heading">
          Prioritas Kecepatan dan Keamananan
        </h3>
        <div className="priority-content-wrapper flex">

          <div className="priority-feature-wrapper">
            
            <div className="priority-feature">
              <h4 className="priority-feature-title">Hosting Super Cepat</h4>
              <p className="priority-description">Pengunjung tidak suka website lambat. Dengan dukungan LiteSpeed Web Server, waktu loading website Anda akan meningkat pesat.</p>
            </div>

            <div className="priority-feature">
              <h4 className="priority-feature-title">Keamananan Website Extra</h4>
              <p className="priority-description">Teknologi keamanan Imunify 360 memungkinkan website Anda terlindung dari serangan hacker, malware, dan virus berbahaya setiap saat.</p>
              <a href="" className="btn orange">lihat selengkapnya</a>
            </div>

            
          </div>	

          <div className="priority-image-wrapper">
            <img src={require("../assets/images/server.webp")} alt="" className="priority-server-img"/>
            <img src={require("../assets/images/graphic.svg")} alt="" className="priority-graphic-img"/>
            <img src={require("../assets/images/imunify.svg")} alt="" className="priority-imunify-img"/>
            <img src={require("../assets/images/lite-speed.svg")} alt="" className="priority-speed-img"/>
          </div>
        </div>
      </div>
    </section>
  )
}

export default PriorityHome;