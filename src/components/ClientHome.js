import React, { useState, Fragment } from 'react';
const ClientHome = ()=> {
  return(
    <section class="client-wrapper">
      <h3 class="heading">Dipercaya 52.000+ Pelanggan di Seluruh Indonesia</h3>

      <div class="container">
        <ul class="client-list-wrapper flex">
          <li class="client"><span class="client-img richeese"></span></li>
          <li class="client"><span class="client-img rabbani"></span></li>
          <li class="client"><span class="client-img otten"></span></li>
          <li class="client"><span class="client-img hydro"></span></li>
          <li class="client"><span class="client-img petro"></span></li>
        </ul>
      </div>
    </section>
  )
}

export default ClientHome;