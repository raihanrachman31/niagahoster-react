import React, { useState, Fragment } from 'react';

const GuaranteeHome = ()=> {
  return(
    <section class="guarantee-wrapper">
      <div class="container">
        <div class="guarantee-content-wrapper flex">
          <div class="guarantee-img-wrapper">
            <img src={require("../assets/images/icons-guarantee.svg")} alt="" class="guarantee-img"/>
          </div>
          
          <div class="guarantee-description-wrapper">
            <h3 class="heading">Garansi 30 Hari Uang Kembali</h3>
            <p class="guarantee-description">Tidak puas dengan layanan hosting Niagahoster? Kami menyediakan garansi uang kembali yang berlaku 30 hari sejak tanggal pembelian.</p>
            <a href="" class="orange">mulai sekarang</a>
          </div>
        </div>
      </div>
    </section>
    
  )
}

export default GuaranteeHome;