import React, { useState } from 'react';

const Example = ()=> {
  const[example,setExample] = useState("Example React")
  return(
    <div>
      <p>{example}</p>
    </div>
  )
}

export default Example;