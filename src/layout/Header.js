import React, { useState } from 'react';
import { Link } from "react-router-dom";

const Header = ()=> {
  return(
      <div className="nav-wrapper">
				<div className="container">
					<nav className="sub-nav">
						<span><i className="fas fa-phone"></i>0274-2885822</span>
						<span><i className="fas fa-comment-alt"></i>Live Chat</span>
						<i className="fas fa-shopping-cart"></i>
					</nav>

					<nav className="main-nav flex">
						<Link to="/" className="nav-brand-wrapper">
							<img src={require("../assets/images/nh-logo.svg")} alt="NIGGAH0STER" className="nav-brand"/>
						</Link>
						<div className="navigation flex">
							<Link to="/unlimited">UNLIMITED HOSTING</Link>
							<Link to="/hosting">CLOUD HOSTING</Link>
							<Link to="/vps">CLOUD VPS</Link>
							<Link to="/domain">DOMAIN</Link>
							<Link to="/afiliasi">AFILIASI</Link>
							<Link to="/blog">BLOG</Link>
							<a href="" className="btn login">LOGIN</a>
						</div>
					</nav>
				</div>
			</div>
  )
}

export default Header;