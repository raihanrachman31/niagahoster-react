import React, { useState, Fragment } from 'react';

const PaymentHome = ()=> {
  return(
    <section class="payment-wrapper">
      <h2 class="payment-title">Beragam pilihan cara pembayaran yang mempercepat proses order hosting & domain Anda.</h2>
      <div class="payment-img-wrapper flex">
        <img src={require("../assets/images/bca.svg")} alt="" class="payment-img"/>
        <img src={require("../assets/images/mandiri.svg")} alt="" class="payment-img"/>
        <img src={require("../assets/images/bni.svg")} alt="" class="payment-img"/>
        <img src={require("../assets/images/bri.svg")} alt="" class="payment-img"/>
        <img src={require("../assets/images/bii.svg")} alt="" class="payment-img"/>
        <img src={require("../assets/images/cimb.svg")} alt="" class="payment-img"/>
        <img src={require("../assets/images/alto.svg")} alt="" class="payment-img"/>
        <img src={require("../assets/images/atm-bersama.svg")} alt="" class="payment-img"/>
        <img src={require("../assets/images/paypal.svg")} alt="" class="payment-img"/>
        <img src={require("../assets/images/indomart.svg")} alt="" class="payment-img"/>
        <img src={require("../assets/images/alfamart.svg")} alt="" class="payment-img"/>
        <img src={require("../assets/images/pegadaian.svg")} alt="" class="payment-img"/>
        <img src={require("../assets/images/pos.svg")} alt="" class="payment-img"/>
        <img src={require("../assets/images/ovo.svg")} alt="" class="payment-img"/>
        <img src={require("../assets/images/gopay.svg")} alt="" class="payment-img"/>
        <img src={require("../assets/images/visa.svg")} alt="" class="payment-img"/>
        <img src={require("../assets/images/master.svg")} alt="" class="payment-img"/>
      </div>
    </section>
  )
}

export default PaymentHome;