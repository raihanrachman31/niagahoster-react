import React, { useState, Fragment, Component } from 'react';

class FaqHome extends Component {
  state = {
    data:[
      {
        id: 1,
        question: "Apa itu web hosting?",
      },
      {
        id: 2,
        question: "Mengapa saya harus menggunakan web hosting Indonesia?",
      },
      {
        id: 3,
        question: "Apa yang dimaksud dengan Unlimited Hosting di Niagahoster?",
      },
      {
        id: 4,
        question: "Paket web hosting apa yang cocok untuk saya?",
      },
      {
        id: 5,
        question: "Apakah semua pembelian mendapatkan domain gratis?",
      },
      {
        id: 6,
        question: "Jika saya sudah memiliki website, apakah saya bisa transfer web hosting ke Niagahoster?",
      },
      {
        id: 7,
        question: "Apa saja layanan web hosting Niaghoster?",
      },
    ]
  }
  render(){
    return(
      <section class="faq-wrapper">
        <h3 class="heading">Pertanyaan yang Sering Diajukan</h3>
        <div class="container">
          <div class="question-wrapper">
            {this.state.data.map( item => {
              return (
                <div>
                  <h3 class="question">{item.question}</h3>
                  <hr/>
                </div>
              )
            })}
            
          </div>
        </div>
      </section>
    )
  }
 }


export default FaqHome;