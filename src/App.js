import React, { useState } from 'react';
import {HomePage, UnlimitedPage, HostingPage, VpsPage, DomainPage, AfiliasiPage, BlogPage} from './routes/Routes'
import Header from './layout/Header'
import Footer from './layout/Footer'
import {Route} from 'react-router-dom';

const App = ()=> {
  return(
    
    <div className="App">
        <Route path ="/" component= {HomePage}  exact/>
        <Route path ="/unlimited" component= {UnlimitedPage}  exact/>
        <Route path ="/hosting" component= {HostingPage}  exact/>
        <Route path ="/vps" component= {VpsPage}  exact/>
        <Route path ="/domain" component= {DomainPage}  exact/>
        <Route path ="/afiliasi" component= {AfiliasiPage}  exact/>
        <Route path ="/blog" component= {BlogPage}  exact/>
    </div>
   
  )
}

export default App;
