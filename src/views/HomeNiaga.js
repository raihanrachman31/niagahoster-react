import React, { useState, Fragment } from 'react';
import HeroHome from '../components/HeroHome'
import ServiceHome from '../components/ServiceHome'
import PriorityHome from '../components/PriorityHome'
import CostSavingHome from '../components/CostSavingHome'
import CustomerHome from '../components/CustomerHome'
import PackageHome from '../components/PackageHome'
import ClientHome from '../components/ClientHome'
import FaqHome from '../components/FaqHome'
import GuaranteeHome from '../components/GuaranteeHome'
import PaymentHome from '../components/PaymentHome'


const HomeNiaga = ()=> {
  const [HomeNiaga, setHomeNiaga] = useState("Home")
  return(
    <div>
      <HeroHome/>
      <main>
        <ServiceHome/>
        <PriorityHome/>
        <CostSavingHome/>
        <CustomerHome/>
        <PackageHome/>
        <ClientHome/>
        <FaqHome/>
        <GuaranteeHome/>
        <PaymentHome/>
      </main>
    </div>
  )
}

export default HomeNiaga;