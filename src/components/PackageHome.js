import React, { useState, Fragment, Component } from 'react';

class PackageHome extends Component {
  state = {
    data: [
     {
      id: 1,
      label: "Termurah!",
      packageName: "Bayi",
      discount: "nodiscount",
      price: "Rp 10.000",
      description: "Sesuai untuk Pemula atau Belajar Website",
      feature: ["500 MB Disk Space","Unlimited Bandwidth","Unlimited Database","1 Domain","Instant Backup","Unlimited SSL Gratis Selamanya"]
     },
     {
      id: 2,
      label: "Diskon up to 34%",
      packageName: "Pelajar",
      discount: "Rp. 60.800,-",
      price: "Rp 40.223",
      description: "Sesuai untuk Budget Minimal, Landing Page, Blog Pribadi",
      feature: ["Unlimited Disk Space","Unlimited Bandwidth","Unlimited POP3 Email","Unlimited Database","10 Addon Domain","Instant Backup","Domain Gratis","Unlimited SSL Gratis Selamanya"]
     },
     {
      id: 3,
      label: "Diskon up to 75%",
      packageName: "Personal",
      discount: "Rp. 106.250,-",
      price: "Rp 26.563",
      description: "Sesuai untuk Website Bisnis, UKM, Organisasi, Komunitas, Toko Online, dll",
      feature: ["Unlimited Disk Space","Unlimited Bandwidth","Unlimited POP3 Email","Unlimited Database","Unlimited Addon Domain","Instant Backup","Domain Gratis","Unlimited SSL Gratis Selamanya","SpamAsassin Mail Protection"]
     },
     {
      id: 4,
      label: "Diskon up to 42%",
      packageName: "Bisnis",
      discount: "Rp. 147.800,-",
      price: "Rp. 85.724",
      description: "Sesuai untuk Website Bisnis, Portal Berita, Toko Online, dll",
      feature: ["Unlimited Disk Space","Unlimited Bandwidth","Unlimited POP3 Email","Unlimited Database","Unlimited Addon Domain","Magic Auto Backup & Restore","Domain Gratis","Unlimited SSL Gratis Selamanya","Prioritas Layanan Support","SpamAsassin Mail Protection"]
     }

    ]
  }
  render(){
    
    return(
      <section class="package-wrapper">
          <h3 class="heading">
            Pilih Paket Hosting Anda
          </h3>
          
          <div class="container">
            <div class="card-wrapper flex">
              {this.state.data.map(item => {
                return (
                  <div class="card package" key={item.id}>
                    <p class="package-label">{item.label}</p>
                    <h5 class="package-name">{item.packageName}</h5>
                    <p class="package-discount">{item.discount}</p>
                    <p class="package-price">{item.price}</p>
                    <hr/>
                    <a href="" class="orange">pilih sekarang</a>
                    <package class="package-description">{item.description}</package>
                    <div class="package-feature-wrapper">
                     {item.feature.map( feature => {
                       return(
                        <p class="package-feature">{feature}</p>
                       )
                     })}
                    </div>
                    <a href="" class="package-feature-detail">Lihat detail fitur</a>
                  </div>
                )
              })}
             
            </div>
          </div>
        </section>

    
    )
  }
}


export default PackageHome;