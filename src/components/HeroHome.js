import React, { useState, Fragment } from 'react';

const HeroHome = ()=> {
  return(
    <header className="document-header-wrapper">
      <section className="contact-header">

        <section className="hero container flex">
          <section className="hero-left">
            <h1 className="hero-title">Unlimited Web Hosting Terbaik di Indonesia</h1>
            <p className="hero-description">Ada banyak peluang bisa Anda raih dari rumah dengan memiliki website. Manfaatkan diskon hosting hingga 75% dan tetap produktif di bulan Ramadhan bersama Niagahoster.</p>
            <br/>
            <p>Yuk segera order karena diskon dapat berakhir sewaktu-waktu!</p>
            <div className="countdown flex">
              
              <p>00</p>
              <p>:</p>
              <p>00</p>
              <p>:</p>
              <p>00</p>
              <p>:</p>
              <p>00</p>

            </div>
            <a href="" className="btn-hero orange">Pilih Sekarang</a>
          </section>
          <section className="hero-right">
            <div className="hero-img-wrapper">
              <img src={require("../assets/images/hero-home-ramadhan.webp")} alt="" className="hero-img"/>
            </div>
          </section>
          
        </section>
      </section>
    </header>
  )
}

export default HeroHome;
//import React, { useState, Fragment } from 'react';
// const HeroHome = ()=> {
//   return(
    
    
//   )
// }

// export default HeroHome;

// import React, { useState, Fragment, Component } from 'react';
// class CostSavingHome extends Component {
//  render(){
//   return(
    
    
//   )
// }
//  }


// export default CostSavingHome;
